import csv
class Player:
	def __init__(self, name, fname):
		self.index = -1
		self.name = name
		self.moves = self.loadMoves(fname)
		
	def	loadMoves(self,fname):
		lst= []
		with open(fname, 'r') as csvfile:
			self.index = 0
			mvReader = csv.reader(csvfile, delimiter=',')
			for row in mvReader:
				lst.append( row)
		return (lst)
				
	def hasMoreMove(self):
		return (self.index < len(self.moves))
		
	def getNextMove(self):
		assert(self.index >=0),"Invalid index"
		if (self.index >= len(self.moves)):
			raise Exception('No more move')
		self.index = self.index +1
		return self.moves[self.index-1]
